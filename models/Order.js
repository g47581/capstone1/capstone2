const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, `Total amount is required`],
    },
    purchasedOn: { 
        type: Date,
        default: new Date()
    },
    userId: {
                type: String,
                required: [true, `userId is required`]
    },
    product: [
            {
                productId:{
                    type:String,
                    required: [true, `ProductID is required`]
                },
                amount:{
                type: Number,
                required: [true, `amount is required`]
                }
            }
    ]
   
    
}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);