const mongoose = require('mongoose');
 

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, `Email is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    // orders:[
    //     {
    //         productId: {
    //             type: String,
    //             required: [true, `Product ID is required`]
    //         },
    //         status: {
    //             type: String,
    //             default: "Purchased",
    //         },
    //         addedOn: {
    //             type: Date,
    //             default: new Date()
    //         }
    //     }
    // ]

}, {timestamps: true})


//Export the model
module.exports = mongoose.model(`User`, userSchema);