const express = require('express');
const router = express.Router();
 
const {
	register,
	getAllUsers,
	login,
	adminStatus,
	userStatus,
	purchased

	
	
} = require('./../controllers/userControllers');
const {verify, decode, verifyAdmin} = require('./../auth');

router.post('/register', async (req, res) => {
	// console.log(req.body)	//user object

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})




//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//LOGIN THE USER
	
router.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



// /isAdmin to update user's isAdmin status to true

router.patch('/isAdmin', verifyAdmin, async (req, res) => {
	try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})


// /isUser to update user's isAdmin status to false
	
router.patch('/isUser', verifyAdmin, async (req, res) => {
	try{
		await userStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// buy a product
router.post('/purchase', verify, async (req, res) => {
	const user = decode(req.headers.authorization).isAdmin
	
	const data ={
		userId: decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	if(user == false){
		try{
			await purchased(data).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}

	} else{
		res.send(`Only with user can buy a product`)
	}	
})







//Export the router module to be used in index.js file
module.exports = router;