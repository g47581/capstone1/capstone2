const express = require('express');
const router = express.Router();
const {
	create, 
	getAProduct,
	getAllProducts,
	updateProduct,
	archive,
	unArchive,
	activeProducts

} = require('./../controllers/productControllers')


const {verifyAdmin, verify} = require('./../auth')


//Create a route "/isActive" to get all active products
	
router.get('/isActive', verify, async (req, res) => {
	try{
		await activeProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route /create a new PRODUCT, 
router.post('/create', verifyAdmin, async (req, res) => {
	// console.log(req.body)
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route "/:productId" to retrieve a specific product
	
router.get('/:productId', verify, async (req, res) => {
	try{
		await getAProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//Create a route "/" to get all the Products, return all documents found
	
router.get('/', verify, async (req, res) => {

	try{
		await getAllProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

//Create a route "/:productId/update" to update product info, return the updated document
router.put('/:productId', verifyAdmin, async (req, res) => {
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//Create a route "/:productId/archive" to update isActive to false, return updated document
	
router.patch('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route "/:productId/unArchive" to update isActive to true, return updated document

router.patch('/:productId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})










//Export the router module to be used in index.js file
module.exports = router;