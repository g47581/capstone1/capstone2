 const express = require('express');
 const router = express.Router();

 const {
 	createOrder,
 	allOrders,
 	getOrder
 	
 
 } = require('./../controllers/orderControllers');

 const {verify, decode, verifyAdmin, verifyUser} = require('./../auth');

// create order

router.post('/checkout', verifyUser, async(req,res)=>{
	
	try {
		await createOrder(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


//retrieve all orders

router.get('/orders', verifyAdmin, async(req,res)=>{
	
	try {
		await allOrders().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//get all orders per users

router.get('/myOrders', verifyUser, async(req,res)=>{
	
	try {
		await getOrder(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})


 //Export the router module to be used in index.js file
module.exports = router;