const Product = require('./../models/Product');

//CREATE A PRODUCT
module.exports.create = async (reqBody) => {
	const {productName, description, price} = reqBody

	let newProduct = new Product({
		productName: productName,
		description: description,
		price: price
	}) 

	return await newProduct.save().then((result, err) => result ? result : err)
}

//GET SPECIFIC product
module.exports.getAProduct = async (id) => {

	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Product not found`}
			}else{
				return err
			}
		}
	})
}


//GET ALL products
module.exports.getAllProducts = async () => {
// console.log('h')
	return await Product.find().then(result => result)
}


//UPDATE A product
module.exports.updateProduct = async (productId, reqBody) => {

	return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
}

//ARCHIVE product
module.exports.archive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new:true}).then(result => result)
}


//UNARCHIVE Product
module.exports.unArchive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new:true}).then(result => result)
}


//ACTIVE PRODUCTS
module.exports.activeProducts = async () => {

	return await Product.find({isActive:true}).then(result => result)
}


