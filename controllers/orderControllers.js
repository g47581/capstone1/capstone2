 const CryptoJS = require("crypto-js");
 const User = require('./../models/User')
 const Product = require('./../models/Product')
 const Order = require('./../models/Order')


 //import createToken function from auth module
 const {createToken} = require('./../auth');


// create an order

module.exports.createOrder = async (reqBody) => { 
	const {userId, productId} = reqBody

	const newOrder = new Order({
		userId: userId,
		productId: productId

	})
	
	const price = Product.findById(productId).then(result => { result.price})
	

	
	return await newOrder.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})

	

}

//get all order

module.exports.allOrders = async () => {
	return await Order.find().then(result => result)
}