const CryptoJS = require("crypto-js");
const User = require('./../models/User')
const Product = require('./../models/Product')
const Order = require('./../models/Order')

//import createToken function from auth module
const {createToken} = require('./../auth');

 
//REGISTER A USER
module.exports.register = async (reqBody) => {
	
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		
	})

	return await  newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}

//GET ALL USERS
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}


//LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password == decryptedPw) //true
				

				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}

//CHANGE TO ADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}



//CHANGE TO ADMIN STATUS TO FALSE
module.exports.userStatus = async (reqBody) => {

	return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
}



// buy a product
module.exports.purchased = async (data) => {
	const {userId, productId} = data

	const updatedUser = await User.findById(userId).then(result => {
		// console.log(result)
		result.orders.push({productId: productId})

		return result.save().then(user => user ? true : false)
	})



	const updatedProduct = await Product.findById(productId).then(result => {
		// console.log(result)
		result.purchasedBy.push({userId: userId})

		return result.save().then(product => product ? true : false)
	})


	if(updatedUser && updatedProduct){
		return true

	} else {
		false
	}


}